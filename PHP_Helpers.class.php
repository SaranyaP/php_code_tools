<?php
/* PHP Parser Helper Functions
 * @author: Jonathan Hilgeman (contact at sitecreative dot com)
 * @updated: 2015-10-30
 * @package: PHP_Code_Tools
 *
 * These are a variety of reusable static functions used for parsing common sections of PHP code.
 */

namespace PHP_Code_Tools;

class PHP_Helpers
{
	// Show surrounding character
	public static function show_char_context(&$pos, &$src, $numContext = 10)
	{
		return str_replace(array("\t","\r","\n"),array("\\t","\\r","\\n"),substr($src,$pos - $numContext,$numContext) . "[" . $src[$pos] . "]" . substr($src,$pos+1,$numContext));
	}

	// See if the char is valid for a name or function (including \ for namespacing)
	public static function is_valid_name_char($char)
	{
		return (!self::is_whitespace($char) && (self::is_alphanumeric($char) || ($char == '_') || ($char == '\\')));
	}

	// See if the char is some kind of valid whitespace
	public static function is_whitespace($char)
	{
		return (($char == "\n") || ($char == "\r") || ($char == ' ') || ($char == "\t"));
	}
	
	// See if the char is either a number or a letter
	public static function is_alphanumeric($char)
	{
		$chr = ord($char);
		return	(		(($chr >= 97) && ($chr <= 122))	// a-z
						||	(($chr >= 48) && ($chr <= 57))	// 0-9
						||	(($chr >= 65) && ($chr <= 90))	// A-Z
						);
	}

	// Find offset of next instance of $find_char, optionally skipping past any quoted values
	public static function find_next($find_char, &$pos, &$src, $skip_over_quoted_values = true)
	{
		$start = $pos;
		$end = strlen($src);
		
		while($pos < $end)
		{
			$char = $src[$pos];
			
			// Check for stop char
			if($char == $find_char)
			{
				return array($start,$pos);
			}
			elseif($skip_over_quoted_values && (($char == '"') || ($char == '\\')))
			{
				// We're skipping past quoted values
				self::read_quoted_value($char, $pos, $src);
			}
			$pos++;
		}
		
		throw new \Exception("Couldn't find any more instances of {$find_char}");
	}
	
	// Find next whitespace char, then the offset of next non-whitespace char OR the next instance of $stop_char
	public static function fast_forward(&$pos, &$src, $stop_char = null)
	{
		$start = $pos;
		$found_whitespace = false;
		$end = strlen($src);
		
		while($pos < $end)
		{
			$char = $src[$pos];
			
			// Check for stop char
			if(($stop_char !== null) && ($char == $stop_char))
			{
				return array($start,$pos);
			}
			
			// Normal search
			if(!$found_whitespace)
			{
				if(self::is_whitespace($char))
				{
					$found_whitespace = true;
				}
			}
			else
			{
				// We've found the whitespace already - now look for the first non-whitespace char
				if(!(self::is_whitespace($char)))
				{
					return array($start,$pos);
				}
			}
			
			$pos++;
		}
	}

	// Find the matching, closing character of an opening one - e.g. the ')' that matches an opening '('
	public static function find_matching_close_char($start_char, &$pos, &$src)
	{
		$skip_over_quoted_values = true;
		$start = $pos;
		$end = strlen($src);

		// Since we could go through multiple levels, make sure we only find the matching char on the starting level
		$level = 0;

		// Determine what our ending char is
		if($start_char == '{') { $end_char = '}'; }
		elseif($start_char == '(') { $end_char = ')'; }
		if(!isset($end_char)) { throw new \Exception("{$start_char} is not handled by _find_matching_close_char"); }
		
		
		// Loop through code
		while($pos < $end)
		{
			$char = $src[$pos];
			
			// $_debug = (($pos > 48100) && ($pos < 48250));
			// if($_debug) { echo __FUNCTION__ . " :: chars = {$start_char}{$end_char}, level = {$level}, {$pos} = " . self::show_char_context($pos,$src) . "\n"; }
			
			// Check for stop char
			if($char == $end_char)
			{
				if($level == 0)
				{
					return array($start,$pos);
				}
				else
				{
					$level--;
				}
			}
			elseif($char == $start_char)
			{
				$level++;
			}
			elseif($skip_over_quoted_values && (($char == '"') || ($char == '\'')))
			{
				// We're skipping past quoted values
				self::read_quoted_value($char, $pos, $src);
			}
			
			$pos++;
		}
		
		throw new \Exception("Couldn't find any more instances of {$end_char}, started at {$start}: " . self::show_char_context($start, $src));
	}
	
	// Read quoted value
	public static function read_quoted_value($quote_char, &$pos, &$src)
	{
		$escaped = false;
		$start = $pos;
		$end = strlen($src);
		
		if(($quote_char != '"') && ($quote_char != '\''))
		{
			throw new \Exception($quote_char . " is not a valid quote character at position {$pos} : " . self::show_char_context($pos, $src));
		}
		
		while($pos < $end)
		{
			$pos++;

			// $debug = (($pos >= 48220) && ($pos < 50000));
			// if($debug) { echo "quote char = {$quote_char} :::: start {$start} = " . self::show_char_context($start, $src) . " :::: pos {$pos} = " . self::show_char_context($pos, $src) . "\n"; }
			
			// Move pointer forward as we parse
			$char = $src[$pos];
			
			if($escaped)
			{
				// Keep going
				$escaped = false;
			}
			elseif($char == '\\')
			{
				// Turn on escape
				$escaped = true;
			}
			elseif($char == $quote_char)
			{
				// End of the quoted value
				$value = substr($src, $start, ($pos-$start + 1));
				return $value;
			}
		}
	}
}
