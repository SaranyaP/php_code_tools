<?php
/* PHP Code Tools Loader
 * @author: Jonathan Hilgeman (contact at sitecreative dot com)
 * @updated: 2015-10-30
 * @package: PHP_Code_Tools
 *
 * This is the starting point for loading the PHP_Code_Tools package. Including
 * this file will enable the usage of the package tools.
 */

require_once(dirname(__FILE__).'/Types.class.php');
require_once(dirname(__FILE__).'/PHP_Helpers.class.php');
require_once(dirname(__FILE__).'/PHP_Parser.class.php');
require_once(dirname(__FILE__).'/PHP_Formatter.class.php');
require_once(dirname(__FILE__).'/PHP_Analyzer.class.php');

// $P = new \PHP_Code_Tools\PHP_Parser(dirname(__FILE__) . "/test.php");
// print_R($P->Results);
// die();