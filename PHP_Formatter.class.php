<?php
/* PHP Formatter v0.1
 * @author: Jonathan Hilgeman (contact at sitecreative dot com)
 * @updated: 2015-10-30
 * @package: PHP_Code_Tools
 *
 * This tool will take the results of a parsed PHP file and convert them back
 * into a formatted PHP string.
 */

namespace PHP_Code_Tools;

class PHP_Formatter
{
	public static function ToPHP(PHP_Code_Container $Container, $depth = 0)
	{
		$output = "";
		$indent = str_repeat("  ",$depth);
		$in_php = !($Container instanceof PHP_Document);
		$LastPart = $Container;
		
		foreach($Container->Parts as $Part)
		{
			if($Part instanceof PHP_NonCodeBlock)
			{
				// If we're in PHP, then close the block
				if($in_php)
				{
					$output .= '?>' . "\n";
					$in_php = false;
				}
				
				// Append the contents
				$output .= $Part->Contents;
				
				// Keep track of last part
				$LastPart = $Part;
				continue;
			}
			
			// If we're re-entering PHP land...
			if(!$in_php)
			{
				$output .= '<?php' . "\n";
				$in_php = true;
			}
			
			if($Part instanceof PHP_StatementBlock)
			{
				foreach($Part->Statements as $phpStatement)
				{
					$output .= $indent . $phpStatement[2] . "\n";
				}
			}
			elseif($Part instanceof PHP_Comment)
			{
				$leading_line_break = ($LastPart instanceof PHP_Control_Block);

				if(substr($Part->Contents,0,2) == '//')
				{
					$output .= ($leading_line_break ? "\n" : "") . $indent . $Part->Contents;
				}
				else
				{
					$output .= "\n" . $indent . $Part->Contents . "\n";
				}
			}
			elseif($Part instanceof PHP_Control_Block)
			{
				$leading_line_break = true; // ($LastPart instanceof PHP_StatementBlock);
				
				// Handle our various control structures (if/else, loops, etc...)
				if($Part->Type == "do..while")
				{
					$output .= ($leading_line_break ? "\n" : "") . $indent . "do\n" . $indent . "{\n";
					$output .= self::ToPHP($Part, $depth + 1);
					$output .= $indent . "} while(" . $Part->Parameters . ");\n\n";
				}
				elseif($Part->Type == "case")
				{
					if($Part->ID == "case default")
					{
						$output .= ($leading_line_break ? "\n" : "") . $indent . "default:\n";
					}
					else
					{
						$output .= ($leading_line_break ? "\n" : "") . $indent . "case " . $Part->Parameters . ":\n";
					}
					$output .= self::ToPHP($Part, $depth + 1);
				}
				else
				{
					$output .= $indent . $Part->Type . "(" . $Part->Parameters . ")\n" . $indent . "{\n";
					$output .= self::ToPHP($Part, $depth + 1);
					$output .= $indent . "}\n\n";
				}
			}
			elseif(($Part instanceof PHP_Function) && ($Container instanceof PHP_Document))
			{
				$output .= $indent . "function " . $Part->Name . "(" . $Part->Parameters . ")\n" . $indent . "{\n";
				$output .= self::ToPHP($Part, $depth + 1);
				$output .= $indent . "}\n";
			}
			elseif($Part instanceof PHP_Class)
			{
				$output .= "\n" . $indent . "class " . $Part->Name . "\n" . $indent . "{\n";
				$output .= self::ToPHP($Part, $depth + 1);
				$output .= $indent . "}\n";
			}
			elseif($Container instanceof PHP_Class)
			{
				if($Part instanceof PHP_Class_Property)
				{
					$output .= $indent . $Part->Contents . "\n";
					continue;
				}
				elseif($Part instanceof PHP_Class_Method)
				{
					$output .= "\n" . $indent . (count($Part->Modifiers) ? implode(" ",$Part->Modifiers) . " " : "") . "function " . $Part->Name . "(" . $Part->Parameters . ")\n" . $indent . "{\n";
					$output .= self::ToPHP($Part, $depth + 1);
					$output .= $indent . "}\n";
				}
			}
			
			// Keep track of last part
			$LastPart = $Part;
		}
		
		return $output;
	}
}
